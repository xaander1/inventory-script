REM :: This is for collecting invetory 
echo off
for /f "tokens=2 delims==" %%a in ('wmic computersystem get name /value') do for %%b in (%%a) do set "comp_name=%%b"
echo -------------------------------------------------------------->>"%comp_name%.txt"
echo %date%>>"%comp_name%.txt"
echo Computer Name: %comp_name% >>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic computersystem get username /value') do for %%b in (%%a) do set "client_name=%%b"
echo Client Name: %client_name%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic computersystem get model /value') do for %%b in (%%a) do set "smodel=%%a"
echo PC/Laptop Model: %smodel%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic bios get serialnumber /value') do for %%b in (%%a) do set "serial=%%a"
echo Serial NO : %serial%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic os get Caption /value') do for %%b in (%%a) do set "os=%%a"
echo Operating System : %os%>>"%comp_name%.txt"
for /f "tokens=3 delims=: " %%a in (
    'cscript //nologo "%systemroot%\system32\slmgr.vbs" /dli ^| find "License Status:"' 
) do set "licenseStatus=%%a"

if /i "%licenseStatus%"=="Licensed" (
  echo Window's Status: Activated>>"%comp_name%.txt"
) else (
  echo Window's Status: Not activated>>"%comp_name%.txt"
)
for /f "tokens=2 delims==" %%a in ('wmic path softwarelicensingservice get OA3xOriginalProductKey /value') do for %%b in (%%a) do set "product_key=%%a"
echo Product Key : %product_key% >>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic cpu get Name /value') do for %%b in (%%a) do set "processor=%%a"
echo Processor : %processor%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic ComputerSystem get TotalPhysicalMemory /value') do for %%b in (%%a) do set "ram=%%a"
echo RAM : %ram%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic logicaldisk get size /value') do for %%b in (%%a) do set "disk_size=%%a"
echo Disk C Size: %disk_size%>>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic logicaldisk where "DeviceID='C:'" get FreeSpace /format:value') do for %%b in (%%a) do set "free_space=%%a"
echo Free Space in C : %free_space%>>"%comp_name%.txt"
:: Get Office Version from File
setlocal enabledelayedexpansion

for /f "tokens=3 delims=." %%a in ('reg query "HKEY_CLASSES_ROOT\Word.Application\CurVer"') do set reg=%%a

set /a i=0
for %%b in (11 12 14 15 16) do (
  if %%b == %reg% goto setver
  set /a i+=1
)

:setver
set /a n=0
for %%c in (2003 2007 2010 2013,2016) do (
  if !n! == !i! set ver=%%c && goto endloop
  set /a n+=1
)

:endloop
echo Microsoft Office Version in Registry: %ver% >> "%comp_name%.txt"
endlocal
:: End
:: Check Office Version
setlocal enableDelayedExpansion
for /f "tokens=2 delims==" %%O in (
    'ftype ^|findstr /r /I "\\OFFICE[0-9]*" 2^>nul') do (
        set "verp=%%~O"
        goto :end_for
)
:end_for
for %%P in (%verp%) do (
        set "off_path=%%~dpP"
        for %%V in ("!off_path:~-3,2!") do (
            set "off_ver=%%~nV"
            call :Map !off_ver! && exit /b
        )
)
:Map
set "v=%1"
set "map=11-2003;12-2007;14-2010;15-2013;16-2016;"
call set v=%%map:*%v%-=%%
set v=%v:;=&rem.%
echo Microsoft Office Version Detected: %v% >> "%comp_name%.txt"
endlocal
:: End Office Version Check
:: Check if activated
setlocal enableDelayedExpansion
for /f "tokens=2 delims==" %%O in ('ftype ^|findstr /r /I "\\OFFICE[0-9]*" 2^>nul') do (
    set "verp=%%~O"
    goto :end_for
)
:end_for

for %%P in (%verp%) do (
    set "off_path=%%~dpP"
    for %%V in ("!off_path:~0,-1!") do (

     set "office_version=%%~nV"
     goto :end_for2
    )
)
:end_for2
if [%office_version%] == [] echo No Office installed >>"%comp_name%.txt" & goto end
echo Microsoft Office Version Verbose: %office_version% >> "%comp_name%.txt"

for /f "tokens=3 delims=: " %%a in (
'cscript "%ProgramFiles%\Microsoft Office\%office_version%\OSPP.VBS" /dstatus ^| find "License Status:"' 

) do set "licenseStatus=%%a"
if /i "%licenseStatus%"=="--- LICENSED ---" (
echo Office Status: Licensed>>"%comp_name%.txt"
) else (
echo Office Status: Not Licenced>>"%comp_name%.txt"
)
:: End Check if activated
echo DONE
echo -------------------------------------------------------------->>"%comp_name%.txt"
pause
