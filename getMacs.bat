REM :: This is for collecting mac addresses 
echo off
for /f "tokens=2 delims==" %%a in ('wmic computersystem get name /value') do for %%b in (%%a) do set "comp_name=%%b"
echo -------------------------------------------------------------->>"%comp_name%.txt"
echo %date%>>"%comp_name%.txt"
echo Computer Name: %comp_name% >>"%comp_name%.txt"
for /f "tokens=2 delims==" %%a in ('wmic computersystem get username /value') do for %%b in (%%a) do set "client_name=%%b"
echo Client Name: %client_name%>>"%comp_name%.txt"
wmic path Win32_NetworkAdapter get name, MacAddress >>"temp.txt"
type temp.txt >> "%comp_name%.txt"
del "temp.txt"
echo -------------------------------------------------------------->>"%comp_name%.txt"